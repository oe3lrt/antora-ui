#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

APP_NPM="$(command -v npm)"
APP_NPX="$(command -v npx)"

#/////////////////////////

set -eu

#/////////////////////////

if [ ! -e "$APP_NPM" ] ; then
    echo "The executable 'npm' wasn't found!"
    exit
fi

if [ ! -e "$APP_NPX" ] ; then
    echo "The executable 'npx' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

echo "--------------------"

# Install dependencies defined in 'package.json'
#echo "Install dependencies defined in 'package.json'"
#$APP_NPM install --quiet --no-progress --cache=.cache/npm
#echo "--------------------"

# Run Gulp
echo "Run Gulp"
node_modules/.bin/gulp preview $@
echo "--------------------"

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////
