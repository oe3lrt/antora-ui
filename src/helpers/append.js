'use strict'

module.exports = (str, suffix) => {
  if (isString(str) && isString(suffix)) {
    return str + suffix
  }
  return str
}

function isString (value) {
  return typeof value === 'string'
}
