#!/usr/bin/env bash

SD=$(cd "$(dirname "$0")"; pwd -P)
WD="$(pwd)"
SCRIPT=$(basename "$0")
SCRIPT_NAME=${SCRIPT%.*}
SCRIPT_EXTENSION=${SCRIPT##*.}
SELF=$SD/$SCRIPT

#/////////////////////////

APP_GIT="$(command -v git)"

#/////////////////////////

set -eu

#/////////////////////////

if [ ! -e "$APP_GIT" ] ; then
    echo "The executable 'git' wasn't found!"
    exit
fi

#/////////////////////////

cd "$SD/" || exit 1

#/////////////////////////

$APP_GIT update-index --chmod=+x "*.sh"
$APP_GIT ls-tree HEAD

#/////////////////////////

cd "$WD/" || exit 1

#/////////////////////////
